#!/usr/bin/env python3

# json-sqlite converter by Attila Tajti

import sqlite3, json

SQLKEY = "@sql"

def expdb(dbfn):
    conn = sqlite3.connect(str(dbfn))
    conn.row_factory = sqlite3.Row

    data, tables, sql = dict(), [], []

    for row in conn.execute("SELECT * FROM sqlite_master"):
        if row['name'].startswith("sqlite"):
            continue
        if row['type'] in ('table', 'view'):
            sql.append(row['sql'] + ";\n")
        if row['type'] == 'table':
            tables.append(row['name'].lower())

    data[SQLKEY] = sql

    idmap = dict()

    # table_info
    #  cid: id of the column
    #  name: the name of the column
    #  type: the type of the column
    #  notnull: 0 or 1 if the column can contains null values
    #  dflt_value: the default value
    #  pk: set is zero for columns that are not part of the primary key, and is the index of the column in the primary key for columns that are part of the primary key.
    tabfmt = {tn: conn.execute("PRAGMA table_info({});".format(tn)).fetchall() for tn in tables}

    while len(tables) != 0:
        proc, prockeys = "", []
        for tablename in tables:
            canprocess = True
            tabkeys = []
            for row in tabfmt[tablename]:
                rowname = row["name"].lower()
                if rowname.endswith("_id") and rowname != tablename + "_id" and rowname[:-3] not in idmap:
                    canprocess = False
                    break
                tabkeys.append(rowname)
            if canprocess:
                proc, prockeys = tablename, tabkeys
                break
        if proc == "":
            raise error("Failed to process tables {}".format(tables))
        tables.remove(proc)
        tablename = proc
        tableids = dict()
        tab = []
        hasid = False
        for row in conn.execute("SELECT * FROM {};".format(tablename)):
            entry = {}
            for k in row.keys():
                k = k.lower()
                if k.endswith('_id'):
                    if k == tablename + "_id":
                        hasid = True
                        myid = row["name"] # must have a name
                        entry['id'] = myid
                        tableids[row[k]] = myid
                    else:
                        ref = k[:-3]
                        entry[k] = idmap[ref][row[k]]
                else:
                    entry[k] = row[k]
            tab.append(entry)
        if hasid:
            xtab = tab
            tab = dict()
            for e in xtab:
                i = e.pop("id")
                tab[i] = e
        else:
            tab.sort(key=lambda e: tuple((e[k] for k in prockeys)))
        idmap[tablename] = tableids
        if len(tab) != 0:
            data[tablename] = tab
    conn.close()
    return data

def impdb(dbfn, data, init=False):
    conn = sqlite3.connect(str(dbfn))
    if init:
        for sql in data[SQLKEY]:
            conn.execute(filtersql(sql))
    cur = conn.cursor()
    for tablename in data:
        if type(data[tablename]) is dict:
            tab = []
            for k, e in data[tablename].items():
                e["id"] = k
                tab.append(e)
            data[tablename] = tab
    tables = [k for k in data.keys() if k.isalnum() and len(data[k]) != 0]
    idmap = dict()
    while len(tables) != 0:
        proc = ""
        for tablename in tables:
            tab = data[tablename]
            e = tab[0]
            canprocess = True
            for k, v in e.items():
                if k.endswith('_id') and k != tablename + '_id' and k[:-3] in tables:
                    canprocess = False
            if canprocess:
                proc = tablename
                break
        if proc == "":
            raise error("Failed to process tables {}".format(tables))
        tables.remove(proc)
        tablename = proc
        tab = data[tablename]
        valuesfmt = ','.join("?" for k in e if k != "id")
        idmap[tablename] = dict()
        for e in tab:
            mapid, keys, values = None, [], []
            for k, v in e.items():
                if k == "id":
                    mapid = v
                else:
                    keys.append(k)
                    if k.endswith("_id"):
                        values.append(idmap[k[:-3]][v])
                    else:
                        values.append(v)
            ins = "INSERT INTO {}({}) VALUES({})".format(tablename, ",".join(keys), valuesfmt)
            cur.execute(ins, values)
            if mapid != None:
                idmap[tablename][mapid] = cur.lastrowid
    conn.commit()
    conn.close()

def filtersql(sql):
    # http://www.sqlite.org/withoutrowid.html
    if sqlite3.sqlite_version < "3.8.2":
        sql = sql.replace("WITHOUT ROWID", "")
    return sql

if __name__ == "__main__":
    import os, argparse
    parser = argparse.ArgumentParser(description="json-sqlite utility")
    parser.add_argument("--db", type=str, help="db to import/export/test")
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument("--import", dest="imp", type=argparse.FileType('r'), help="json file to import")
    group.add_argument("--export", dest="exp", type=argparse.FileType('w'), help="json file to export")
    group.add_argument("--test", action="store_true", help="run self-test")
    parser.add_argument("--init", action="store_true", help="initialize db using sql in json file for import")
    parser.add_argument("--drop", nargs='*', help="tables to drop on export")
    parser.add_argument("--xdb", type=str, default="test.sqlite", help="scratch db file name for testing")
    parser.add_argument("--xjson", type=str, default="test.json", help="scratch json file name for testing")
    arg = parser.parse_args()

    if arg.imp:
        data = json.loads(arg.imp.read())
        impdb(arg.db, data, init=arg.init)
    elif arg.exp:
        data = expdb(arg.db)
        for drop in arg.drop:
            data.pop(drop, None)
        arg.exp.write(json.dumps(data, sort_keys=True, indent=2, separators=(',', ': ')))
    else:
        data = expdb(arg.db)
        jsondata0 = json.dumps(data, sort_keys=True, indent=2, separators=(',', ': '))
        with open(arg.xjson, 'w') as f:
            f.write(jsondata0)
        if os.path.exists(arg.xdb):
            os.remove(arg.xdb)
        with open(arg.xjson) as f:
            data = json.loads(f.read())
        impdb(arg.xdb, data, True)
        data = expdb(arg.db)
        jsondata1 = json.dumps(data, sort_keys=True, indent=2, separators=(',', ': '))
        if jsondata0 != jsondata1:
            print("First and second export are different")
        else:
            print("First and second exports are the same")


