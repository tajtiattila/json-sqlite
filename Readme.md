
json-sqlite converter by Attila Tajti

Database prerequisites
----------------------

* primary key should be tablename + "_id" or empty
* tables with primary key must have a name column

Usage
-----

Export my.sqlite into a file called my.json

  `json-sqlite --db my.sqlite --exp my.json`

Import a formerly created my.json into my.sqlite:

  `json-sqlite --db my.sqlite --imp my.json`

Import my.json to a new db (in this case my.sqlite should not exist):

  `json-sqlite --db my.sqlite --init --imp my.json`

Self-Test to export, then import into scratch db, finally export
again and to see if the two exports are the same. (Specified db
will not be modified)

  `json-sqlite --db my.sqlite --test`
